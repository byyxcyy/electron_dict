"""
数据库模块:
    将数据库操作封装一个类, 哪里需要往哪里搬
ps:
    数据库需要提前安装好, 配置好 库 表 字段, 否则无法运行!!!
    字典文件位于当前目录下的dict.txt文档, 需要自己写个循环写进数据库
    相应的 库 表 字段 已在readme.md文档中有写
"""
import pymysql
import hashlib


class DataBase:
    def __init__(self, host, port, user, passwd, charset, database):
        """
        建立到MySQL数据库的连接。接受一些参数:
        kwargs:
        :param host:   数据库地址
        :param port:   数据库端口
        :param user:   数据库用户名
        :param passwd:   数据库密码
        :param charset:   数据库编码格式
        :param database:   数据库
        """
        self.db = pymysql.connect(host=host, port=port,
                                  user=user, password=passwd,
                                  charset=charset, database=database)
        # print(kwargs)
        # self.__connect_database()

    def close(self):
        """重写关闭"""
        self.db.close()

    def cursor_(self):
        """重写游标"""
        self.cur = self.db.cursor()

    def register(self, name, passwd):
        """注册"""
        sql = f"select * from user where name='{name}';"
        self.cur.execute(sql)
        r = self.cur.fetchone()  # 如果查找不到,返回None
        if r:
            return "有相同的用户名,请重新输入!"

        sql = f"insert into user (name, passwd) values ('{name}', '{passwd}')"
        try:
            self.cur.execute(sql)
            self.db.commit()
            return 'ok'
        except Exception:
            self.db.rollback()  # 回退上一步
            return "创建账户失败!"

    def login(self, name, passwd):
        """登录"""
        # mi = self.__encryption(name, passwd)
        sql = f"select * from user where name='{name}' and passwd='{passwd}'"
        self.cur.execute(sql)
        r = self.cur.fetchall()
        if r:
            return 'ok'
        else:
            return "账号或密码错误!"

    def __encryption(self, name, passwd):
        hash = hashlib.md5(name.encode())  # 加盐
        hash.update(passwd.encode())  # 加密
        return hash.hexdigest()

    def query(self, word):
        """单词查询"""
        sql = f"select mean from words where word='{word}';"
        self.cur.execute(sql)
        r = self.cur.fetchall()  # 如果查找不到,返回None
        if r:
            return r[0][0]  # 找到了返回是一个元组,取第一个才是单词的解释

    def insert_hist(self, name, word):
        """插入历史记录"""
        sql = f"insert into hist (name, word) values ('{name}', '{word}');"
        try:
            self.cur.execute(sql)
            self.db.commit()
        except Exception:
            self.db.rollback()  # 回退
            print("历史记录插入失败!")

    def do_log(self, name):
        """历史记录"""
        # order by time desc limit 10 逆序排序取出最新10条数据
        sql = f"select name,word,time from hist where name='{name}' order by time desc limit 10;"
        self.cur.execute(sql)
        return self.cur.fetchall()  # 为空返回None
